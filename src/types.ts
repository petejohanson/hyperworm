
export interface StatItem {
    type: string;
    label: string;
    value: number;
}

export interface Performance {
    tick(): StatItem[];
    refresh(): void;
}

export interface State {
    canvas: HTMLCanvasElement | null;
    playing: boolean;
    universe: import('hyperworm-engine').Universe;
    stats: StatItem[];
    performance: Performance;
}