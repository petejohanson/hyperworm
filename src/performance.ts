

import { StatItem } from './types';

export class Performance {
    private frames: number[];
    private lastTimestamp: number;

    public constructor() {
        this.frames = [];
        this.lastTimestamp = performance.now();
    }

    public refresh(): void {
        this.lastTimestamp = performance.now();
    }

    public tick(): StatItem[] {
        const now = performance.now();
        const delta = now - this.lastTimestamp;
        this.lastTimestamp = now;
        const fps = 1 / delta * 1000;

        // Save only the latest 100 timings.
        this.frames.push(fps);
        if (this.frames.length > 100) {
            this.frames.shift();
        }

        // Find the max, min, and mean of our 100 latest timings.
        let min = Infinity;
        let max = -Infinity;
        let sum = 0;
        for (let i = 0; i < this.frames.length; i++) {
            sum += this.frames[i];
            min = Math.min(this.frames[i], min);
            max = Math.max(this.frames[i], max);
        }
        let mean = sum / this.frames.length;

        return [
            { type: "last", label: "Latest", value: fps },
            { type: "mean", label: "Mean", value: mean },
            { type: "minimum", label: "Max", value: max },
            { type: "maximum", label: "Min", value: min },
        ];
    }
}