import { h, app, ActionFunc, ActionResult, SubscriptionsResult, VNode } from 'hyperapp';
import { State } from './types';
import { animate } from './subs/animation';
import { observe, EffectData } from './subs/dom';
import { Stats } from './comps/stats';
import { Performance as Perf } from './performance';

async function go(): Promise<void> {
    const { Universe, Direction, GameStatus } = await import('hyperworm-engine');

    const node = document.getElementById("app") || undefined;

    function draw(universe: import('hyperworm-engine').Universe, canvas: HTMLCanvasElement): void {
        universe.draw_to(canvas.getContext('2d'));
    }

    const KeyMapping: { [key: string]: import('hyperworm-engine').Direction } = {
        "ArrowDown": Direction.Down,
        "ArrowUp": Direction.Up,
        "ArrowLeft": Direction.Left,
        "ArrowRight": Direction.Right,
    }

    const GameKeyDown: ActionFunc<State, KeyboardEvent> = (state: State, { key }: KeyboardEvent): ActionResult<State, KeyboardEvent> => {
        const { universe, performance } = state;
        let newDirection: import('hyperworm-engine').Direction;

        if (key === " ") {
            performance.refresh();
            state = { ...state, playing: !state.playing };
        } else if ((newDirection = KeyMapping[key]) !== null) {
            universe.set_direction(newDirection);
        }

        return state;
    };

    const CanvasDom: ActionFunc<State, EffectData> = (state: State, { element: canvas }: EffectData): ActionResult<State, EffectData> => {
        if (canvas instanceof HTMLCanvasElement) {
            if (canvas !== null) {
                draw(state.universe, canvas);
            }
    
            return { ...state, canvas };
        }

        return state;
    };

    const Tick: ActionFunc<State, DOMHighResTimeStamp> = (state: State, /*, _timestamp: DOMHighResTimeStamp */): ActionResult<State, DOMHighResTimeStamp> => {
        const { universe, canvas, performance: perf, playing } = state;

        let status = universe.tick();
        if (canvas !== null) {
            draw(universe, canvas);
        }

        let newPlaying = playing;
        if (status != GameStatus.Active) {
            newPlaying = false;
        }

        return { ...state, stats: perf.tick(), playing: newPlaying };
    };

    const observeProps = { select: { id: 'universe' }, target: document.body, action: CanvasDom };

    app<State>({
        init: {
            canvas: null,
            playing: false,
            universe: Universe.new(),
            performance: new Perf(),
            stats: [],
        },
        view: (state: State): VNode<object> => (
            <main>
                <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                    <canvas
                        width={state.universe.get_canvas_width()}
                        height={state.universe.get_canvas_height()}
                        id="universe"
                        onkeydown={GameKeyDown}
                        tabindex="1"
                    >
                    </canvas>
                    <Stats items={state.stats}></Stats>
                </div>
            </main>
        ),
        subscriptions: (state: State): SubscriptionsResult<State> => [
            observe<State>(observeProps),
            state.playing && animate<State>(Tick),

        ],
        node
    });
}

go();
