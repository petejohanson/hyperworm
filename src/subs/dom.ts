import { Action, Dispatch, Subscription } from "hyperapp";

// import { State } from '../types';

export interface ObserveOptions<State> {
    select: {};
    target: Element;
    action: Action<State, EffectData>;
    config?: MutationObserverInit;
}

export type EffectData = ElementAdded | ElementRemoved | ElementUpdated;

export interface ElementAdded {
    type: "added";
    element: Element;
}

export interface ElementRemoved {
    type: "removed";
    element: Element;
}

export interface ElementUpdated {
    type: "updated";
    element: Element;
    attribute: [any, string];
}

// DOM Effect
// Observer Implementation
// Will be moved to subscriptions package
const find = (query: { [key: string]: string | null }, nodes: ArrayLike<Node> & Iterable<Node>, cb: (element: Element) => void): void => {
    for (let node of nodes) {
        if (node instanceof Element) {
            let elem = node;
            // if(node.nodeType !== 1 ) return
            if(Object.keys(query).every((key: string): boolean => elem.getAttribute(key) === query[key]))
                return cb(node)
            else if(node.childNodes.length)
                return find(query, node.childNodes, cb)
        }
    }
};

const onChange = (select: {}, cb: (data: EffectData) => void): MutationCallback => (mutations): void => {
    mutations.forEach(
        ({ target, type, attributeName, oldValue, addedNodes, removedNodes }): void => {

            type === 'attributes' && attributeName &&
                find(select, [target], (element: Element): void =>
                    cb({ type: 'updated', element, attribute: [oldValue, attributeName] }))

            addedNodes.length &&
                find(select, addedNodes, (element: Element): void => cb({ type: 'added', element }))

            removedNodes.length &&
                find(select, removedNodes, (element: Element): void => cb({ type: 'removed', element }))
        });
};

function observeEffect<State>(dispatch: Dispatch<State>, options: unknown): () => void {
    const { select, target, action, config } = options as ObserveOptions<State>;
    
    const observer = new MutationObserver(
        onChange(select, (data: EffectData): void => dispatch(action, data))
    )
    observer.observe(target, {
        attributes: true, childList: true, subtree: true, ...config
    });
    return (): void => observer.disconnect();
}

export function observe<State>(props: ObserveOptions<State>): Subscription<State> {
    return [observeEffect, props];
}