import { Action, Dispatch, Subscription } from 'hyperapp';

function animateEffect<State>(dispatch: Dispatch<State>, options: unknown): (() => void) {
    const action = options as Action<State, DOMHighResTimeStamp>
    let cancelId: number;

    function frame(timestamp: DOMHighResTimeStamp): void {
        dispatch(action, timestamp);
        cancelId = requestAnimationFrame(frame);
    }

    cancelId = requestAnimationFrame(frame);

    return (): void => { cancelAnimationFrame(cancelId) };
}

export function animate<State>(action: Action<State, DOMHighResTimeStamp>): Subscription<State> {
    return [animateEffect, action];
}
