import { h } from 'hyperapp';

import { StatItem } from '../types';

function Stat(item: StatItem): JSX.Element[] {
    return [
        <dh>{item.label}</dh>,
        <dt>{Math.round(item.value) || ""}</dt>
    ];
}

export function Stats({ items }: { items: StatItem[] }): JSX.Element {
    return (
        <dl>
            {items.map(Stat)}
        </dl>
    );
}