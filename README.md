# HyperWorm

[![pipeline status](https://gitlab.com/petejohanson/hyperworm/badges/master/pipeline.svg)](https://gitlab.com/petejohanson/hyperworm/commits/master)

A basic implementation of a worm game, using HyperApp V2, TypeScript, and Rust + WebAssembly.

## Status

HyperWorm is pre-alpha software! It should be considered usable for experimentation/learning purposes only.

## Docker Image

To test play the latest version, run the following once you have Docker installed:

```console
$ docker run --name hyperworm -d -p 8080:80 registry.gitlab.com/petejohanson/hyperworm:latest
```

And then load [http://localhost:8080](http://localhost:8080) in your browser.

## Controls

The following controls will work after focusing the canvas area. Improvements to keyboard input
will hopefully come soon.

* Space - Play/Pause the game
* Up/Down/Left/Right - Control the direction of the worm

## Local Development

### Prerequisites

* Rust via [rustup](https://rustup.rs/)
* [wasm-pack](https://rustwasm.github.io/wasm-pack/)
* [NodeJS](https://nodejs.org/en/)

### Building/Running

First, build the Rust project

```console
$ wasm-pack build engine
```

Then you can install Node modules and run locally:

```console
$ npm ci
$ npm start
```

You can then view the app at [http://localhost:8080](http://localhost:8080)