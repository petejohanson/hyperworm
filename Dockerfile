FROM rustlang/rust:nightly-slim AS RUST_BUILD

RUN apt-get update && apt-get -y install curl && \
    curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh

# create a new empty shell project
RUN USER=root cargo new --lib my-project
WORKDIR /my-project

# copy over your manifests
COPY ./engine/Cargo.lock ./Cargo.lock
COPY ./engine/Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN wasm-pack build --release
RUN find src/ -name "*.rs" | xargs rm

RUN find ./target -name "*hyperworm*" | xargs rm -rf
RUN rm -rf ./pkg

# build for release

COPY ./engine/src ./src

RUN wasm-pack build --release

FROM node:lts-alpine as JS_BUILD

# TODO: Remove once we're not using hyperapp from Git
RUN apk add --no-cache git

COPY package.json package-lock.json ./
RUN npm ci

COPY --from=RUST_BUILD my-project/pkg engine/pkg

COPY .babelrc webpack.config.js tsconfig.json ./
COPY ./src ./src

RUN npm run build

FROM nginx:alpine

COPY docker/nginx.conf /etc/nginx/nginx.conf

COPY --from=JS_BUILD dist/ /usr/share/nginx/html
