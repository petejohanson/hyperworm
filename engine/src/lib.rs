
#![feature(euclidean_division)]

extern crate wasm_bindgen;
extern crate rand;

mod timer;

use timer::Timer;

use rand::Rng;

use std::f64;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell {
    Empty,
    Food(u32),
    Body(u32)
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Direction {
    Stopped = 0,
    Up = 1,
    Down = 2,
    Left = 3,
    Right = 4
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Speed {
    Normal = 3,
    Fast = 2,
    Faster = 1,
    Fastest = 0
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum GameStatus {
    Active = 1,
    Done = 2
}

#[wasm_bindgen]
#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct CellLocation {
    row: u32,
    column: u32
}

#[wasm_bindgen]
pub struct Universe {
    tick_count: u8,
    speed: Speed,
    height: u32,
    width: u32,
    head: CellLocation,
    worm_length: u32,
    direction: Direction,
    cells: Vec<Cell>,
}

const CELL_SIZE: f64 = 10.0;
const MAX_FOOD_AGE: u32 = 500;
const START_WORM_LENGTH: u32 = 10;
const FOOD_GROWTH_FACTOR: u32 = 5;

fn cell_iter(width: u32, height: u32) -> impl Iterator<Item = CellLocation> {
    (0..width).flat_map(move |row| (0..height).map(move |column| CellLocation { row, column }))
}

#[wasm_bindgen]
impl Universe {
    pub fn new() -> Universe {
        let tick_count = 0;
        let speed = Speed::Fast;
        let height = 40;
        let width = 40;
        let worm_length = START_WORM_LENGTH;
        let head = CellLocation { row: 5, column: 5 };
        let direction = Direction::Stopped;

        let cells = cell_iter(width, height).map(|_| Cell::Empty).collect();

        Universe {
            height,
            width,
            speed,
            tick_count,
            head,
            worm_length,
            direction,
            cells
        }
    }

    pub fn set_direction(&mut self, direction: Direction) -> () {
        self.direction = direction;
    }

    pub fn tick(&mut self) -> GameStatus {
        self.tick_count = (self.tick_count + 1).rem_euclid(self.speed as u8);

        if self.tick_count != 0 || self.direction == Direction::Stopped {
            return GameStatus::Active;
        }

        let _timer = Timer::new("Universe::tick");
        let CellLocation { row: head_row, column: head_col } = self.head;

        let next_head = match self.direction.clone() {
            Direction::Down => CellLocation { row: (head_row + 1).rem_euclid(self.height), column: head_col },
            Direction::Up => CellLocation { row: (head_row + (self.width - 1)).rem_euclid(self.height), column: head_col },
            Direction::Left => CellLocation { row: head_row, column: (head_col + (self.width - 1)).rem_euclid(self.width) },
            Direction::Right => CellLocation { row: head_row, column: (head_col + 1).rem_euclid(self.width) },
            _ => CellLocation { row: head_row, column: head_col },
        };

        // TODO: Store rng?
        let new_food = if rand::rngs::OsRng::new().unwrap().gen_range(0, 500) == 250 { Some(self.random_empty_location()) } else { None };

        let mut game_status = GameStatus::Active;
        let new_cells = self.iter().map(|cl| {
            let current = self.cell_at(cl);

            match current {
                Cell::Empty => {
                    if cl == next_head {
                        Cell::Body(0)
                    } else if Some(cl) == new_food {
                        Cell::Food(0)
                    } else {
                        Cell::Empty
                    }
                },
                Cell::Food(food_age) => {
                    if cl == next_head {
                        self.worm_length = self.worm_length + FOOD_GROWTH_FACTOR;
                        Cell::Body(0)
                    } else if food_age > &MAX_FOOD_AGE {
                        Cell::Empty
                    } else {
                        Cell::Food(food_age + 1)
                    }
                },
                Cell::Body(body_position) => {
                    if cl == next_head {
                        game_status = GameStatus::Done;
                        Cell::Body(body_position.clone())
                    } else if body_position < &self.worm_length {
                        Cell::Body(body_position + 1)
                    } else {
                        Cell::Empty
                    }
                }
            }
        }).collect();

        if game_status == GameStatus::Done {
            self.direction = Direction::Stopped;
        }

        self.cells = new_cells;
        self.head = next_head;

        game_status
    }

    fn iter(&self) -> impl Iterator<Item = CellLocation> {
        cell_iter(self.width, self.height)
    }

    fn random_empty_location(&self) -> CellLocation {
        let mut rng = rand::rngs::OsRng::new().unwrap();

        std::iter::repeat_with(|| CellLocation { row: rng.gen_range(0, self.width), column: rng.gen_range(0, self.height) })
            .skip_while(|c| self.cell_at(*c) != &Cell::Empty)
            .next()
            .unwrap()
    }

    pub fn get_width(&self) -> u32 {
        self.width
    }

    pub fn get_height(&self) -> u32 {
        self.height
    }

    fn cell_at(&self, loc: CellLocation) -> &Cell {
        &self.cells[self.get_index(loc)]
    }

    fn get_index(&self, loc: CellLocation) -> usize {
        (self.width * loc.row + loc.column) as usize
    }

    pub fn get_canvas_height(&self) -> f64 {
        self.height as f64 * CELL_SIZE
    }

    pub fn get_canvas_width(&self) -> f64 {
        self.width as f64 * CELL_SIZE
    }

    pub fn draw_to(&self, context: &web_sys::CanvasRenderingContext2d) -> () {
        let _timer = Timer::new("Universe::draw_to");
        context.clear_rect(0.0, 0.0, self.width as f64 * CELL_SIZE, self.height as f64 * CELL_SIZE);
        context.begin_path();

        self.iter().for_each(|loc| {
            match self.cell_at(loc) {
                Cell::Empty => {

                },
                Cell::Food(_) => {
                    // TODO: DON'T DO THIS CRAZY!
                    context.stroke();
                    context.begin_path();
                    let top = CELL_SIZE * (loc.row as f64);
                    let left = CELL_SIZE * (loc.column as f64);
                    context.set_stroke_style(&JsValue::from_str("red"));
                    context.move_to(left + CELL_SIZE / 4.0, top + CELL_SIZE / 2.0);
                    context
                        .ellipse(left + CELL_SIZE / 2.0, top + CELL_SIZE / 2.0, CELL_SIZE / 4.0, CELL_SIZE / 4.0, 0.0, f64::consts::PI, f64::consts::PI * 3.0)
                        .unwrap();
                    context.stroke();
                    context.set_stroke_style(&JsValue::from_str("black"));
                    context.begin_path();
                },
                Cell::Body(_) => {
                    let top = CELL_SIZE * (loc.row as f64);
                    let left = CELL_SIZE * (loc.column as f64);
                    context.move_to(left + CELL_SIZE / 4.0, top + CELL_SIZE / 2.0);
                    context
                        .ellipse(left + CELL_SIZE / 2.0, top + CELL_SIZE / 2.0, CELL_SIZE / 4.0, CELL_SIZE / 4.0, 0.0, f64::consts::PI, f64::consts::PI * 3.0)
                        .unwrap();
                }
            }
        });

        context.stroke();
    }
}
